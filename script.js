const container = document.querySelector(".container");
const jumbo = document.querySelector(".jumbo");
const kecils = document.querySelectorAll(".kecil");

container.addEventListener("click", function(e) {
    if( e.target.className == "kecil" ) {
        jumbo.src = e.target.src;
        jumbo.classList.add("fade");
        setTimeout(function() { 
            jumbo.classList.remove("fade");
        }, 500);


        kecils.forEach(function(AR) {
           if( AR.classList.contains("active")) {
                AR.classList.remove("active");
           };
        });

        e.target.classList.add("active");
    }
});